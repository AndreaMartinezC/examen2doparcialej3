﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio3Parcial2
{
    class Program
    {
        static void Main(string[] args)
        {
            float[] arr = { 2, 34.5f, 1, 103.453f, 45 };
            InsertionSort.Sort(arr, arr.Length);
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine("{0 }", arr[i]);
            }
        }
    }
}
